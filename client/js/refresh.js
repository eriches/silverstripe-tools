// refresh the page at first load. Can use for the silverstripe-dropdown2autocomplete in the cms, because this field is only displayed well after refresh
// insert within the getCMSFields function: Requirements::javascript("zzz_admin/js/refresh.js");

(function()
{
    if( window.localStorage )
    {
        if( !localStorage.getItem( 'firstLoad' ) )
        {
            localStorage[ 'firstLoad' ] = true;
            window.location.reload();
        }
        else
            localStorage.removeItem( 'firstLoad' );
    }
})();
