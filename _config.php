<?php

use SilverStripe\View\Parsers\ShortcodeParser;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;

ShortcodeParser::get('default')->register('YOUTUBE_LIGHT', ['ShortCodesExtension', 'YoutubeLight']);
ShortcodeParser::get('default')->register('NOP_YOUTUBELIGHT', ['ShortCodesExtension', 'YoutubeLight']);

$formats = [
    [ 'title' => 'Cards', 'items' => [
        [
            'title' => 'quote',
            'block' => 'blockquote',
            'classes' => 'qcard blockquote-inl mt-5 mb-5',
            'wrapper' => true
        ],
        [
            'title' => 'emphasize',
            'block' => 'div',
            'classes' => 'qcard emphasize mt-5 mb-5',
            'wrapper' => true
        ],
        [
            'title' => 'note',
            'block' => 'div',
            'classes' => 'notecard mt-5 mb-5',
            'wrapper' => true
        ],
        [
            'title' => 'alert',
            'block' => 'div',
            'classes' => 'alertcard mt-5 mb-5',
            'wrapper' => true
        ]
    ]
    ],
    [ 'title' => 'Misc styles', 'items' => [
        [
            'title' => 'small',
            'inline' => 'small'
        ],
        [
            'title' => 'green span',
            'inline' => 'span',
            'classes' => 'font-green'
        ],
        [
            'title' => 'red span',
            'inline' => 'span',
            'classes' => 'font-red'
        ],
        [
            'title' => 'green td',
            'selector' => 'td',
            'classes' => 'font-green'
        ],
        [
            'title' => 'red td',
            'selector' => 'td',
            'classes' => 'font-red'
        ],
        [
            'title' => 'link blue',
            'selector' => 'a',
            'classes' => 'lb'
        ],
        [
            'title' => 'h-smaller',
            'selector' => 'h2,h3',
            'classes' => 'h-smaller'
        ],
        [
            'title' => 'h-small',
            'selector' => 'h2,h3',
            'classes' => 'h-small'
        ],
        [
            'title' => 'content-1',
            'selector' => 'p',
            'classes' => 'content-1'
        ],
        [
            'title' => 'fw-bold',
            'inline' => 'span',
            'classes' => 'fw-bold'
        ],
        [
            'title' => 'fw-900',
            'inline' => 'span',
            'classes' => 'fw-900'
        ],
        [
            'title' => 'hlt span',
            'inline' => 'span',
            'classes' => 'hlt'
        ],
        [
            'title' => 'hlt strong',
            'inline' => 'strong',
            'classes' => 'hlt'
        ],
        [
            'title' => 'hlt-above (H)',
            'selector' => 'h2,h3,h4,h5,h6',
            'classes' => 'hlt-above'
        ],
        [
            'title' => 'hlt-under (H)',
            'selector' => 'h2,h3,h4,h5,h6',
            'classes' => 'hlt-under'
        ],
        [
            'title' => 'hlt-above (span)',
            'inline' => 'span',
            'classes' => 'hlt-above'
        ],
        [
            'title' => 'hlt-under (span)',
            'inline' => 'span',
            'classes' => 'hlt-under'
        ]
    ]
    ],
    [ 'title' => 'Bootstrap table', 'items' => [
        [
            'title' => 'table-bordered',
            'selector' => 'table',
            'classes' => 'table table-bordered'
        ],
        [
            'title' => 'table-striped',
            'selector' => 'table',
            'classes' => 'table table-striped'
        ],
        [
            'title' => 'table-striped-columns',
            'selector' => 'table',
            'classes' => 'table table-striped'
        ],
        [
            'title' => 'not full width',
            'selector' => 'table',
            'classes' => 'w-auto'
        ],
        [
            'title' => 'table-responsive (add first!!)',
            'block' => 'div',
            'classes' => 'table-responsive',
            'wrapper' => true,
        ]
    ]
    ],
    [ 'title' => 'Button', 'items' => [
        [
            'title' => 'btn-lg',
            'selector' => 'a',
            'classes' => 'btn-lg'
        ],
        [
            'title' => 'btn-info',
            'selector' => 'a',
            'classes' => 'btn-info'
        ],
        [
            'title' => 'cta-button',
            'selector' => 'a',
            'classes' => 'btn-info btn-lg d-table-cell'
        ],
        [
            'title' => 'cta button wrapper',
            'block' => 'div',
            'classes' => 'd-flex justify-content-center',
            'wrapper' => true
        ],
        [
            'title' => 'fa-arrow-right',
            'inline' => 'span',
            'classes' => 'fa-regular fa-arrow-right ml-2'
        ],
        [
            'title' => 'ml-1',
            'selector' => 'a',
            'classes' => 'ml-1'
        ],
        [
            'title' => 'mr-1',
            'selector' => 'a',
            'classes' => 'mr-1'
        ]
    ]
    ],
    [
        'title' => 'List styles', 'items' => [
        [
            'title' => 'list-mb',
            'selector' => 'ul,ol',
            'classes' => 'list-mb',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'arrow-right',
            'selector' => 'ul',
            'classes' => 'list-arrow-right',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'check',
            'selector' => 'ul',
            'classes' => 'list-check',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'hand',
            'selector' => 'ul',
            'classes' => 'list-hand',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'dot',
            'selector' => 'ul',
            'classes' => 'list-dot',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'dot-sm',
            'selector' => 'ul',
            'classes' => 'list-dot-sm',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'angle-right',
            'selector' => 'ul',
            'classes' => 'list-angle-right',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'thumbs-up',
            'selector' => 'ul',
            'classes' => 'list-thumbs-up',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'plus-cirlce',
            'selector' => 'ul',
            'classes' => 'list-plus-cirlce',
            'wrapper' => true,
            'merge_siblings' => false,
        ],
        [
            'title' => 'number-1',
            'selector' => 'ol',
            'classes' => 'list-number-1',
            'wrapper' => true,
            'merge_siblings' => false,
        ]
    ]
    ],
    [
        'title' => 'resposive', 'items' => [
        [
            'title' => 'hide xs sm md',
            'block' => 'div',
            'classes' => 'd-none d-lg-block',
            'wrapper' => true
        ],
        [
            'title' => 'hide xs sm',
            'block' => 'div',
            'classes' => 'd-none d-md-block',
            'wrapper' => true
        ]
    ]
    ]
];

TinyMCEConfig::get('cms')
    ->addButtonsToLine(1, 'styles')
    ->setOptions([
        'importcss_append' => true,
        'style_formats' => $formats,
        'style_formats_merge' => true,
        'extended_valid_elements' => 'small'
    ]);
