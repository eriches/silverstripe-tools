<div id="$HolderID" class="field form-check<%-- if extraClass %> $extraClass<% end_if --%>">
	$Field
	<label class="form-check-label" for="$ID">$Title</label>
	<% if $Message %><span class="message $MessageType">$Message</span><% end_if %>
	<% if $Description %><span class="description">$Description</span><% end_if %>
</div>
