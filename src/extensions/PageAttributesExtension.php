<?php

namespace Hestec\Tools;

use SilverStripe\Core\Extension;
use SilverStripe\Forms\FieldList;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextField;
use SilverStripe\Core\Config\Config;

class PageAttributesExtension extends Extension {

    private static $db = array(
        'RatingTitle' => 'Varchar(255)',
        'RatingVotes' => 'Int',
        'RatingScore' => 'Int',
        'RatingEnabled' => 'Boolean',
        'RefreshCach' => 'Boolean'
    );

    private static $has_one = array(
        'ShareImageLandscape' => Image::class,
        'ShareImageSquare' => Image::class
    );

    private static $owns = array(
        'ShareImageLandscape',
        'ShareImageSquare'
    );

    public function updateCMSFields(FieldList $fields) {

        $ShareImageLandscapeField = new UploadField('ShareImageLandscape', 'ShareImageLandscape');
        $ShareImageLandscapeField->allowedExtensions = array('jpg', 'png', 'gif');
        $ShareImageLandscapeField->setDescription("1200 x 630");
        $ShareImageLandscapeField->setFolderName("share/ls");

        $ShareImageSquareField = new UploadField('ShareImageSquare', 'ShareImageSquare');
        $ShareImageSquareField->allowedExtensions = array('jpg', 'png', 'gif');
        $ShareImageSquareField->setDescription("1200 x 1200");
        $ShareImageSquareField->setFolderName("share/sq");

        $RatingEnabledField = CheckboxField::create('RatingEnabled', "RatingEnabled");
        $RatingVotesField = NumericField::create('RatingVotes', "RatingVotes");
        $RatingScoreField = NumericField::create('RatingScore', "RatingScore");

        $RatingTitleField = TextField::create('RatingTitle', "RatingTitle");

        $RefreshCachField = CheckboxField::create('RefreshCach', "RefreshCach");
        $RefreshCachField->setDescription("Dit zorgt ervoor dat LastEdited wijzigt en daardoor een nieuwe cache wordt gezet. Na opslaan gaat dit vinkje direct weer uit (onAfterWrite).");

        $fields->addFieldsToTab("Root.Main", array(
            $RefreshCachField
        ));

        $fields->addFieldsToTab("Root.SocialShare", array(
            $ShareImageLandscapeField,
            $ShareImageSquareField
        ));

        if (Config::inst()->get('RatingTab') && Config::inst()->get('RatingTab') == true){
            $fields->addFieldsToTab("Root.Rating", array(
                $RatingEnabledField,
                $RatingTitleField,
                $RatingVotesField,
                $RatingScoreField
            ));
        }

    }

    public function onBeforeWrite() {

        if (isset($this->owner->Content) && !empty($this->owner->Content)){

            $this->owner->Content = preg_replace('/<p>((?=\[NOP_).*?])<\/p>/m', '$1', $this->owner->Content);

        }

    }

    public function onAfterWrite() {

        if ($this->owner->RefreshCach == true){

            $this->owner->RefreshCach = false;
            $this->owner->write();

        }

    }

}
