<?php

namespace Hestec\Tools;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Admin\CMSMenu;
use SilverStripe\Core\Config\Config;

class CMSHelpLink extends DataExtension {
	
	function init() {

        // unique identifier for this item. Will have an ID of Menu-$ID
        $id = 'Help';

        // your 'nice' title
        $title = 'Help';

        // the link you want to item to go to
        $link = 'https://www.hestecsupport.nl/index.php?/Knowledgebase/List/Index/4/silverstripe-cms';

        // priority controls the ordering of the link in the stack. The
        // lower the number, the lower in the list
        $priority = -2;

        // Add your own attributes onto the link. In our case, we want to
        // open the link in a new window (not the original)
        $attributes = array(
            'target' => '_blank'
        );

        CMSMenu::remove_menu_item('Help');
        CMSMenu::add_link($id, $title, $link, $priority, $attributes);
    }
	
}