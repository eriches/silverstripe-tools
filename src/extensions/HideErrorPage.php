<?php

namespace Hestec\Tools;

use SilverStripe\Core\Extension;

class HideErrorPage extends Extension {

    private static $hide_ancestor = 'SilverStripe\ErrorPage\ErrorPage';

}
