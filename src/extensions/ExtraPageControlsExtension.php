<?php

namespace Hestec\Tools;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Extension;
use Detection\MobileDetect;
use SilverStripe\Control\Director;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Config\Config;
use Spatie\SchemaOrg\Schema;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Security\Security;

class ExtraPageControlsExtension extends Extension {

    public function isFirefox(){

        if (strlen(strstr($_SERVER['HTTP_USER_AGENT'], 'Firefox')) > 0) {
            return true;
        }
        return false;

    }

    // add TestMobileInFirefox: true to mysite.yml and Firefox is detected as mobile
    public function TestMobileInFirefox(){

        if (Config::inst()->get('TestMobileInFirefox') == true) {
            return true;
        }
        return false;

    }

    // Detect tablets & mobile phones
    public function isMobile(){
        $detect = new MobileDetect();
        // Any mobile device (phones or tablets).
        if ( $detect->isMobile() ) {
            return true;
        }
        return false;
    }

    // Detect tablets
    // add TestMobileInFirefox: true to mysite.yml and Firefox is detected as mobile
    public function isTablet(){
        $detect = new MobileDetect;
        // Any tablet device.
        if( $detect->isTablet() || (Director::isDev() && $this->owner->isFirefox() && $this->owner->TestMobileInFirefox()) ){
            return true;
        }
        return false;
    }
    // Detect tablets & mobile phones
    // add TestMobileInFirefox: true to mysite.yml and Firefox is detected as mobile
    public function isPhone(){
        $detect = new MobileDetect;
        // Exclude tablets.
        if( ($detect->isMobile() && !$detect->isTablet()) || (Director::isDev() && $this->owner->isFirefox() && $this->owner->TestMobileInFirefox()) ){
            return true;
        }
        return false;
    }

    public function getDevice(){
        $detect = new MobileDetect;
        if ($detect->isTablet()){
            return "tablet";
        }
        if ($detect->isPhone()){
            return "phone";
        }
        return "computer";
    }

    // Detect ifDev
    public function isDev(){
        if( Director::isDev() ){
            return true;
        }
        return false;
    }

    // Detect ifLive
    public function isLive(){
        if( Director::isLive() ){
            return true;
        }
        return false;
    }

    // No partial cache if logged in or in dev
    public function noCache(){
        if( Director::isDev() || Security::getCurrentUser()) {
            return true;
        }
        return false;
    }

    // TimeVersion used to give Less.js (included in themes/templates/Page.ss) a version number
    public function TimeVersion(){
        return time();
    }

    // get the language in the template with $LangFromLocale for example: gets "en" from "en_US"
    public function getLangFromLocale() {
        return i18n::get_lang_from_locale(i18n::get_locale());
    }

    public function CharacterCount($string){
        return strlen($string);
    }

    public function CanonicalUrl(){

        if (in_array($_SERVER['HTTP_HOST'], Config::inst()->get('ExtraCaconicalUrls'))) {

            $canonical_base = "https://".$_SERVER['HTTP_HOST'];

        }else{

            $canonical_base = Environment::getEnv('SS_CANONICAL_BASE_URL');

        }

        if (filter_var($canonical_base, FILTER_VALIDATE_URL)) {

            $url = $canonical_base . strtok($_SERVER["REQUEST_URI"], '?');

            // url must always have a trailing slash
            if (substr($url, -1) != '/') {
                $url = $url . "/";
            }
            return $url;
        }
        return false;

    }

    public function HreflangPath(){

        $url = strtok($_SERVER["REQUEST_URI"], '?');

        // url must always have a trailing slash
        if (substr($url, -1) != '/') {
            $url = $url . "/";
        }
        return $url;

    }

    // geoip functie gaf deze fout bij en dev/build:
    //[Tue Sep 27 15:17:55 2016] [error] [client 90.145.111.13] PHP Parse error:  syntax error, unexpected '[' in /home/w4l39/domains/39.w4l.nl/public_html/zzz_admin/code/extensions/ExtraPageControlsExtension.php on line 72

    /*public function getGeoipCountryCode() {

        if (function_exists('geoip_record_by_name')) {

            //return geoip_record_by_name('52.63.2.69')['country_code']; // test, this ip is from Australia, so must return: AU
            return geoip_record_by_name($_SERVER['REMOTE_ADDR'])['country_code'];

        }else{

            return false; // geoip library not installed in PHP, see Hestec wiki how to install

        }

    }*/

    public function RatingAverageScore(){

        if ($this->owner->RatingScore > 0 && $this->owner->RatingVotes > 0) {
            $output = round($this->owner->RatingScore / $this->owner->RatingVotes, 1);
            return number_format($output, 1, '.', '');
        }
        return false;

    }

    public function SchemaAggregateRating($score = null, $votes = null){

        $RatingScore = $this->owner->RatingScore;
        $RatingAverageScore = $this->RatingAverageScore();
        $RatingVotes = $this->owner->RatingVotes;
        if ($score > 0 && $votes > 0){
            $RatingScore = $score;
            $RatingVotes = $votes;
            $RatingAverageScore = round($RatingScore / $RatingVotes, 1);
            $RatingAverageScore = number_format($RatingAverageScore, 1, '.', '');
        }

        if ($this->owner->RatingEnabled && $RatingScore > 0 && $RatingVotes > 0){

            $aggregateRating = Schema::aggregateRating();
            $aggregateRating->worstRating(1);
            $aggregateRating->bestRating(10);
            $aggregateRating->ratingValue($RatingAverageScore);
            $aggregateRating->ratingCount($RatingVotes);

            return $aggregateRating;

        }

        return false;

    }

    public function SchemaOrganization(){

        $siteconfig = SiteConfig::current_site_config();

        $organizationname = SiteConfig::current_site_config()->Title;
        if (SiteConfig::current_site_config()->OrganizationName && strlen(SiteConfig::current_site_config()->OrganizationName) > 2){
            $organizationname = SiteConfig::current_site_config()->OrganizationName;
        }

        $org_connections = array();
        if (SiteConfig::current_site_config()->CheckLinkedinUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->LinkedinUrl);
        }
        if (SiteConfig::current_site_config()->CheckFacebookUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->FacebookUrl);
        }
        if (SiteConfig::current_site_config()->CheckTwitterUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->TwitterUrl);
        }
        if (SiteConfig::current_site_config()->CheckInstagramUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->InstagramUrl);
        }
        if (SiteConfig::current_site_config()->CheckThreadsUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->ThreadsUrl);
        }
        if (SiteConfig::current_site_config()->CheckBlueskyUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->BlueskyUrl);
        }
        if (SiteConfig::current_site_config()->CheckMastodonUrl()){
            array_push($org_connections, SiteConfig::current_site_config()->MastodonUrl);
        }

        $logo = Schema::imageObject();
        $logo->url(SiteConfig::current_site_config()->OrganizationLogo()->AbsoluteURL);
        $logo->width(SiteConfig::current_site_config()->OrganizationLogo()->Width);
        $logo->height(SiteConfig::current_site_config()->OrganizationLogo()->Height);

        $address = Schema::postalAddress();
        $address->streetAddress($siteconfig->Address);
        $address->addressLocality($siteconfig->City);
        $address->postalCode($siteconfig->Zip);

        $organization = Schema::organization();
        $organization->name($organizationname);
        if ($siteconfig->Address && $siteconfig->City && $siteconfig->Zip){
            $organization->address($address);
        }
        $organization->telephone($siteconfig->Phone);
        $organization->logo($logo);
        if (!empty($org_connections)) {
            $organization->sameAs($org_connections);
        }

        return $organization;

    }

    public function SchemaWebsite(){

        $baseurl = Environment::getEnv('SS_CANONICAL_BASE_URL').'/';
        $homepage = \Page::get()->filter(array('URLSegment' => 'home'))->first();

        $website = Schema::webSite();
        $website->identifier($baseurl.'#website');
        $website->url($baseurl);
        $website->name($homepage->MetaTitle);
        $website->description($homepage->MetaDescription);
        $website->publisher($this->owner->SchemaOrganization());

        return $website;

    }

    public function SchemaWebpage($url = null)
    {

        $pageurl = $this->owner->AbsoluteLink();
        if (filter_var($url, FILTER_VALIDATE_URL)){
            $pageurl = $url;
        }

        $webpage = Schema::webpage();
        $webpage->identifier($pageurl.'#webpage');
        $webpage->url($pageurl);
        if (method_exists($this->owner,'NewContentLocale')){
            $webpage->inLanguage($this->owner->NewContentLocale());
        }else{
            $webpage->inLanguage($this->owner->ContentLocale());
        }
        $webpage->name($this->owner->MetaTitle);
        $webpage->description($this->owner->MetaDescription);
        $webpage->datePublished($this->owner->Created);
        $webpage->dateModified($this->owner->LastEdited);
        if (isset($this->owner->PublishDate)){
            $webpage->datePublished($this->owner->PublishDate);
            if (isset ($this->owner->LastUpdated) && $this->owner->LastUpdated > $this->owner->PublishDate){
                $webpage->dateModified($this->owner->LastUpdated);
            }else{
                $webpage->dateModified($this->owner->PublishDate);
            }
        }
        $webpage->isPartOf($this->SchemaWebsite());

        return $webpage;

    }

    public function generateURLSegment($segment)
    {

        if ($segment){

            return SiteTree::create()->generateURLSegment($segment);

        }

        return false;

    }

}
