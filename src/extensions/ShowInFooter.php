<?php

namespace Hestec\Tools;

use SilverStripe\ORM\DataExtension;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;

class ShowInFooter extends DataExtension {

    private static $db = array(
        'ShowInFooter' => 'Boolean(1)'
    );

    private static $has_one = array(
    );

    //function getSettingsFields() {
    //    $fields = parent::getSettingsFields();
    //    $fields->addFieldToTab("Root.Settings", new CheckBoxField('ShowFooter', _t("Page.ShowFooter","Show in footer?")), 'ShowInSearch');
    //    return $fields;
    //}

    public function updateSettingsFields(FieldList $fields) {
        //$fields = parent::getCMSFields();

        $fields->addFieldToTab("Root.Settings", new CheckBoxField('ShowInFooter', _t("SiteTree.SHOWINFOOTER","Show in footer?")), 'ShowInSearch');
        return $fields;
    }

    public function FullMenu(){

        if (class_exists('Translatable')) {

            return SiteTree::get()->filter(array('ParentID' => 0, 'Locale' => $this->owner->Locale));

        }else{

            return SiteTree::get()->filter(array('ParentID' => 0));

        }

    }

}
