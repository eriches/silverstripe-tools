<?php

use SilverStripe\ORM\DataExtension;

class BsFormField extends DataExtension {

    public function prependIcon($icon) {
        $this->owner->PreIcon = $icon;

        return $this;
    }

    public function appendIcon($icon) {
        $this->owner->ApIcon = $icon;

        return $this;
    }

    public function prependText($text) {
        $this->owner->PreText = $text;

        return $this;
    }

    public function appendText($text) {
        $this->owner->ApText = $text;

        return $this;
    }


}