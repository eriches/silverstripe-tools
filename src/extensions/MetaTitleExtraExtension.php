<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\View\ArrayData;
use SilverStripe\View\HTML;
use SilverStripe\View\SSViewer;
use Kinglozzer\SilverStripeMetaTitle\MetaTitleExtension;
use SilverStripe\Control\Controller;

class MetaTitleExtraExtension extends DataExtension
{

    private static $title_format = '$MetaTitle &raquo; $SiteConfig.Title';

    public function MetaTags(&$tags)
    {

        if (method_exists(Controller::curr(), 'newMetaDescription') && Controller::curr()->newMetaDescription()){

            $newDescriptionTag = HTML::createTag('meta', [
                'name' => 'description',
                'content' => Controller::curr()->newMetaDescription()
            ]);
            $tags = preg_replace("/<meta name=\"description\" content=\"(.+)\" \/>/i", $newDescriptionTag, $tags);

        }
        if (method_exists(Controller::curr(), 'newMetaTitle') && Controller::curr()->newMetaTitle()){

            $format = Config::inst()->get(static::class, 'title_format');

            $data = ArrayData::create([
                'MetaTitle' => Controller::curr()->newMetaTitle()
            ]);

            //$newTitleTag = HTML::createTag('title', [], Controller::curr()->newMetaTitle());
            $newTitleTag = HTML::createTag('title', [], SSViewer::execute_string($format, $data));
            $tags = preg_replace("/<title>(.+)<\/title>/i", $newTitleTag, $tags);

        }

    }
}
