<?php

namespace Hestec\Tools;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Permission;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Translatable;

class CustomSiteConfig extends DataExtension {

    private static $db = array(
        'FooterContent' => 'HTMLText',
        'GATrackingCode' => 'Varchar',
        'GTMTrackingCode' => 'Varchar',
        'GlobalFromEmail' => 'Varchar(255)',
        'GlobalFromName' => 'Varchar(255)',
        'FacebookUrl' => 'Varchar(255)',
        'TwitterUrl' => 'Varchar(255)',
        'InstagramUrl' => 'Varchar(255)',
        'YoutubeUrl' => 'Varchar(255)',
        'LinkedinUrl' => 'Varchar(255)',
        'PinterestUrl' => 'Varchar(255)',
        'ThreadsUrl' => 'Varchar(255)',
        'BlueskyUrl' => 'Varchar(255)',
        'MastodonUrl' => 'Varchar(255)',
        'SnapchatUrl' => 'Varchar(255)',
        'SoundcloudUrl' => 'Varchar(255)',
        'FacebookAppId' => 'Varchar',
        'TawkSiteId' => 'Varchar',
        'AdCrowdPixel' => 'Varchar',
        'FooterColumnTitle1' => 'Varchar',
        'FooterColumnTitle2' => 'Varchar',
        'FooterColumnTitle3' => 'Varchar',
        'FooterColumnTitle4' => 'Varchar',
        'FooterColumnTitle5' => 'Varchar',
        'FooterColumnTitle6' => 'Varchar',
        'FooterContent2' => 'HTMLText',
        'FooterContent3' => 'HTMLText',
        'FooterContent4' => 'HTMLText',
        'OrganizationName' => 'Varchar(255)',
        'DefaultAuthorFirstName' => 'Varchar(255)',
        'DefaultAuthorSurname' => 'Varchar(255)',
        'DefaultAuthorSocial' => 'Varchar(255)',
        'Address' => 'Varchar(255)',
        'City' => 'Varchar(255)',
        'Zip' => 'Varchar(20)',
        'Phone' => 'Varchar(20)',
    );

    private static $has_one = array(
        'OrganizationLogo' => Image::class
    );

    private static $owns = array(
        'OrganizationLogo'
    );

    private static $has_many = array(
        'OwnIps' => OwnIp::class,
        'BrandColors' => 'BrandColor',
        'BrandColorTypes' => 'BrandColorType',
        'ExtraNavigationItems' => 'ExtraNavigationItem'
    );

    public function updateCMSFields(FieldList $fields)
    {

        if (!Permission::check('ADMIN')){
            $fields->removeByName(array(
                'Access'
            ));
            $fields->removeFieldFromTab('Root.Main', 'Theme');
        }

        $ExtraNavigationItemsField = new GridField(
            'ExtraNavigationItems',
            _t("SiteConfig.NAVIGATION_ITEMS", "Navigation items"),
            $this->owner->ExtraNavigationItems(),
            GridFieldConfig_RecordEditor::create()
        /*->addComponent(new GridFieldToolbarHeader())
        ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
        ->addComponent(new GridFieldSortableHeader())
        ->addComponent(new GridFieldDataColumns())
        ->addComponent(new GridFieldPaginator(50))
        ->addComponent(new GridFieldEditButton())
        ->addComponent(new GridFieldDeleteAction())
        ->addComponent(new GridFieldDetailForm())
        ->addComponent(new GridFieldFilterHeader())*/
        //->addComponent(new GridFieldOrderableRows('SortOrder'))
        );

        $OwnIpsGridField = new GridField(
            'OwnIps',
            _t("SiteConfig.OWN_IPS", "Own ip's"),
            $this->owner->OwnIps(),
            GridFieldConfig_RecordEditor::create()
        /*->addComponent(new GridFieldToolbarHeader())
        ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
        ->addComponent(new GridFieldSortableHeader())
        ->addComponent(new GridFieldDataColumns())
        ->addComponent(new GridFieldPaginator(50))
        ->addComponent(new GridFieldEditButton())
        ->addComponent(new GridFieldDeleteAction())
        ->addComponent(new GridFieldDetailForm())
        ->addComponent(new GridFieldFilterHeader())*/
        //->addComponent(new GridFieldOrderableRows('SortOrder'))
        );
        $OwnIpsInfoField = LiteralField::create('OwnIpsInfo', '<p>'._t("OwnIp.OWN_IP_INFO", "Own ip adresses can be used for exclude functions like Analytics, dependent on your site configuration.").'</p>');

        /*$conf=GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldOrderableRows('SortOrder'));
        $conf->removeComponentsByType('GridFieldPaginator');
        $conf->removeComponentsByType('GridFieldPageCount');*/

        $GlobalFromEmailField = EmailField::create("GlobalFromEmail", _t("SiteConfig.GLOBAL_FROM_EMAIL","Global from email"));
        $GlobalFromNameField = TextField::create("GlobalFromName", _t("SiteConfig.GLOBAL_FROM_NAME","Global from name"));
        $GlobalFromNameField->setDescription(_t("SiteConfig.GLOBAL_FROM_NAME_DESCRIPTION","The name which is displayed with the from email"));

        $GLOBALCONTENTINFO = "The content fields below are for content that is not linked to a specific page, such as texts in the header and footer. Not all fields may apply to this website.";
        $GlobalContentInfoField = LiteralField::create('GlobalContentInfoField', '<p>'._t("SiteConfig.GLOBALCONTENTINFO",$GLOBALCONTENTINFO).'</p>');
        $FooterCmsHeaderField = HeaderField::create('FooterCmsHeaderField', 'Footer content');
        $FooterContentField = HTMLEditorField::create("FooterContent", _t("SiteConfig.FooterContent","Content column")." 1");
        $FooterContentField->setRows(15);
        $FooterContent2Field = HTMLEditorField::create("FooterContent2", _t("SiteConfig.FooterContent","Content column")." 2");
        $FooterContent2Field->setRows(15);
        $FooterContent3Field = HTMLEditorField::create("FooterContent3", _t("SiteConfig.FooterContent","Content column")." 3");
        $FooterContent3Field->setRows(15);
        $FooterContent4Field = HTMLEditorField::create("FooterContent4", _t("SiteConfig.FooterContent","Content column")." 4");
        $FooterContent4Field->setRows(15);
        $FooterColumnTitle1Field = TextField::create("FooterColumnTitle1", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 1");
        $FooterColumnTitle2Field = TextField::create("FooterColumnTitle2", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 2");
        $FooterColumnTitle3Field = TextField::create("FooterColumnTitle3", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 3");
        $FooterColumnTitle4Field = TextField::create("FooterColumnTitle4", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 4");
        $FooterColumnTitle5Field = TextField::create("FooterColumnTitle5", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 5");
        $FooterColumnTitle6Field = TextField::create("FooterColumnTitle6", _t("SiteConfig.FOOTERCOLUMNTITLE","Column title")." 6");

        //$googeanalyticsinfofield = LiteralField::create("googeanalyticsinfofield", "<h2>Google Analytics</h2>");
        $gatrackingcodefield = TextField::create("GATrackingCode", "Google Analytics tracking code");
        $GTMtrackingcodefield = TextField::create("GTMTrackingCode", "Google Tag Manager tracking code");
        $facebookappidfield = TextField::create("FacebookAppId", "Facebook app ID");
        $tawksiteidfield = TextField::create("TawkSiteId", "Tawk site ID");
        $AdCrowdPixelfield = TextField::create("AdCrowdPixel", "AdCrowd pixel");
        $AdCrowdPixelfield->setDescription(_t("SiteConfig.ADCROWDPIXEL_INFO", 'The code in the pixel URL, between "//pixel.adcrowd.com/smartpixel/" and ".js".'));

        $socialmediainfofield = LiteralField::create("socialmediainfofield", _t("SiteConfig.SOCIALMEDIAINFO","Start the URL's with <strong>http://</strong> or <strong>https://</strong>."));
        $facebookurlfield = TextField::create("FacebookUrl", "Facebook URL");
        $twitterurlfield = TextField::create("TwitterUrl", "Twitter URL");
        $instagramurlfield = TextField::create("InstagramUrl", "Instagram URL");
        $youtubeurlfield = TextField::create("YoutubeUrl", "Youtube URL");
        $linkedinurlfield = TextField::create("LinkedinUrl", "LinkedIn URL");
        $pinteresturlfield = TextField::create("PinterestUrl", "Pinterest URL");
        $ThreadsUrlField = TextField::create("ThreadsUrl", "Threads URL");
        $BlueskyUrlField = TextField::create("BlueskyUrl", "Bluesky URL");
        $MastodonUrlField = TextField::create("MastodonUrl", "Mastodon URL");
        $snapchaturlfield = TextField::create("SnapchatUrl", "Snapchat URL");
        $soundcloudfield = TextField::create("SoundcloudUrl", "Soundcloud URL");

        $OrganizationLogoField = UploadField::create('OrganizationLogo', _t("Schemadata.ORGANIZATION_LOGO", "Organization logo"));
        $OrganizationLogoField->setDescription(_t("Schemadata.MINIMUM_SIZE", "Minimum size 160 x 90 px."));
        $OrganizationLogoField->setFolderName("schemadata");
        $OrganizationNameField = TextField::create('OrganizationName', _t("Schemadata.ORGANIZATION_NAME", "Organization name"));
        $DefaultAuthorHeaderField = HeaderField::create('DefaultAuthorHeader', _t("Schemadata.DEFAULTAUTHOR", "Default author"));
        $DefaultAuthorFirstNameField = TextField::create('DefaultAuthorFirstName', _t("Schemadata.DEFAULT_AUTHOR_FIRSTNAME", "First name"));
        $DefaultAuthorSurnameField = TextField::create('DefaultAuthorSurname', _t("Schemadata.DEFAULT_AUTHOR_SURNAME", "Surname"));
        $DefaultAuthorSocialField = TextField::create('DefaultAuthorSocial', _t("Schemadata.DEFAULT_AUTHOR_SOCIAL", "Social media profile"));
        $DefaultAuthorSocialField->setDescription(_t("Schemadata.DEFAULT_AUTHOR_SOCIAL_DESCRIPTION", "URL to social media profile"));
        $AddressField = TextField::create('Address', _t("Schemadata.ADDRESS", "Address"));
        $CityField = TextField::create('City', _t("Schemadata.CITY", "City"));
        $ZipField = TextField::create('Zip', _t("Schemadata.ZIP", "Zip"));
        $PhoneField = TextField::create('Phone', _t("Schemadata.PHONE", "Phone"));

        $fields->addFieldsToTab("Root.Main", array(
            $GlobalFromEmailField,
            $GlobalFromNameField,
        ));

        $fields->addFieldsToTab("Root.Footer", array(
            $FooterCmsHeaderField,
            $GlobalContentInfoField,
            $FooterColumnTitle1Field,
            $FooterColumnTitle2Field,
            $FooterColumnTitle3Field,
            $FooterColumnTitle4Field,
            $FooterColumnTitle5Field,
            $FooterColumnTitle6Field,
            $FooterContentField,
            $FooterContent2Field,
            $FooterContent3Field,
            $FooterContent4Field
        ));

        $fields->addFieldsToTab("Root."._t("SiteConfig.EXTRA_NAVIGATION", "Extra Navigation"), array(
            $ExtraNavigationItemsField
        ));

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.CONNECTIONS", "Connections"), array(
                $gatrackingcodefield,
                $GTMtrackingcodefield,
                $facebookappidfield,
                $tawksiteidfield,
                $AdCrowdPixelfield
            ));
        }

        $fields->addFieldsToTab("Root."._t("SiteConfig.SOCIAL_MEDIA", "Social Media"), array(
            $socialmediainfofield,
            $facebookurlfield,
            $twitterurlfield,
            $instagramurlfield,
            $youtubeurlfield,
            $linkedinurlfield,
            $pinteresturlfield,
            $ThreadsUrlField,
            $BlueskyUrlField,
            $MastodonUrlField,
            $snapchaturlfield,
            $soundcloudfield
        ));

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.SCHEMA_DATA", "Schema data"), array(
                $OrganizationLogoField,
                $OrganizationNameField,
                $AddressField,
                $CityField,
                $ZipField,
                $PhoneField,
                $DefaultAuthorHeaderField,
                $DefaultAuthorFirstNameField,
                $DefaultAuthorSurnameField,
                $DefaultAuthorSocialField
            ));
        }

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.OWN_IPS", "Own ip's"), array(
                $OwnIpsInfoField,
                $OwnIpsGridField
            ));

            /*$fields->addFieldsToTab("Root.BrandColors", array(
                $BrandColorsField
            ));*/

        }

    }

    /*public function validate() {
        $result = parent::validate();
        if(strlen($this->Label) > 28 || strlen($this->Label) < 2) {
            $result->error(_t("ExtraNavigation.LABEL_VALIDATION","The label must be between 2 and 28 characters (include spaces)."));
        }
        if(!filter_var($this->ExternLink, FILTER_VALIDATE_URL) && !empty($this->ExternLink)) {
            $result->error(_t("ExtraNavigation.EXTERNAL_LINK_VALIDATION","External link"));
        }
        return $result;
    }*/

    public function GlobalFromEmail() {

        if ($output = $this->owner->GlobalFromEmail){
            return $output;
        }else{
            return "noreply@hestec.email";
        }

    }

    public function CheckFacebookUrl() {

        if (filter_var($this->owner->FacebookUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckTwitterUrl() {

        if (filter_var($this->owner->TwitterUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckInstagramUrl() {

        if (filter_var($this->owner->InstagramUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckYoutubeUrl() {

        if (filter_var($this->owner->YoutubeUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckLinkedinUrl() {

        if (filter_var($this->owner->LinkedinUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckPinterestUrl() {

        if (filter_var($this->owner->PinterestUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckThreadsUrl() {

        if (filter_var($this->owner->ThreadsUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckBlueskyUrl() {

        if (filter_var($this->owner->BlueskyUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckMastodonUrl() {

        if (filter_var($this->owner->MastodonUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckSnapchatUrl() {

        if (filter_var($this->owner->SnapchatUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function CheckSoundcloudUrl() {

        if (filter_var($this->owner->SoundcloudUrl, FILTER_VALIDATE_URL)){
            return true;
        }else{
            return false;
        }

    }

    public function isNotOwnIp() {

        if ($ownips = OwnIp::get()) {
            foreach ($ownips as $ip){
                if (str_starts_with($_SERVER['REMOTE_ADDR'], $ip->IpAddress)){
                    return false;
                }
            }
        }
        return true;

    }

}
