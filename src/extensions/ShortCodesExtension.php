<?php

class ShortCodesExtension  {

    public static function YoutubeLight($arguments)
    {

        if (isset($arguments['id'])) {

            if (empty($arguments['title'])) {
                $arguments['title'] = '';
            }
            if (empty($arguments['width'])) {
                $arguments['width'] = 720;
            }
            if (empty($arguments['height'])) {
                $arguments['height'] = round($arguments['width'] / 1.777777777777778);
            }

            $script = '<div style="width:' . $arguments['width'] . 'px;height:' . $arguments['height'] . 'px" class="ytdefer" data-alt="' . $arguments['title'] . '" data-title="' . $arguments['title'] . '" data-src="' . $arguments['id'] . '"></div>';
            $script .= "<script>window.addEventListener('load', ytdefer_setup);</script>";
            return $script;

        }
        return '';
    }

    public function MarkDownReplace($content){

        $search = array(
            '{b}',
            '{/b}',
            '{i}',
            '{/i}',
            '{u}',
            '{/u}',
            '{s}',
            '{/s}'
        );

        $replace = array(
            '<strong>',
            '</strong>',
            '<em>',
            '</em>',
            '<u>',
            '</u>',
            '<small>',
            '</small>'
        );

        $output = str_replace($search, $replace, $content);
        return $output;

    }

}
