<?php

use SilverStripe\Security\Permission;
use SilverStripe\ORM\DataObject;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\CMS\Model\SiteTree;

class ExtraNavigationItem extends DataObject
{
	private static $singular_name = 'Extra Navigation Item';
    private static $plural_name   = 'Extra Navigation Items';

	private static $db = array (
		'Label' => 'Varchar(28)',
		//'Icon' => 'Varchar(255)',
		'ExternLink' => 'Varchar(255)',
		'SortOrder' => 'Int',
	);

	private static $has_one = array (
		'InternLink' => 'SiteTree',
		'SiteConfig' => SiteConfig::class
	);

	private static $summary_fields = array(
		'Label' => 'Label'
	);

	private static $default_sort =  'SortOrder ASC';
	
	public function getCMSFields()
	{

		$LabelField = TextField::create('Label', 'Label');
		//$IconField = FontAwesomeIconPickerField::create('Icon', 'Icoon');
		$InternLinkField = TreeDropdownField::create('InternLinkID', _t("ExtraNavigation.INTERNAL_LINK","Internal link"), SiteTree::class);
		$ExternLinkField = TextField::create('ExternLink', _t("ExtraNavigation.EXTERNAL_LINK","External link"));
		$ExternLinkField->setDescription(_t("ExtraNavigation.EXTERNAL_LINK_DESCRIPTION","External link will be use if filled. Always start with http:// or https:// !"));

		$fields =  new FieldList(
			$LabelField,
			//$IconField,
			$InternLinkField,
			$ExternLinkField
		);
		
		return $fields;
	}

	public function validate() {
		$result = parent::validate();
		if(strlen($this->Label) > 28 || strlen($this->Label) < 2) {
			$result->error(_t("ExtraNavigation.LABEL_VALIDATION","The label must be between 2 and 28 characters (include spaces)."));
		}
		if(!filter_var($this->ExternLink, FILTER_VALIDATE_URL) && !empty($this->ExternLink)) {
			$result->error(_t("ExtraNavigation.EXTERNAL_LINK_VALIDATION","External link"));
		}
		return $result;
	}

	public function canView($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	public function canEdit($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	public function canDelete($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	/*public function canCreate($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}*/
}