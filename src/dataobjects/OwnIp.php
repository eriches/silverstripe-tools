<?php

namespace Hestec\Tools;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\ORM\DataObject;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;

class OwnIp extends DataObject {

    private static $singular_name = 'Own ip address';
    private static $plural_name = 'Own ip addresses';

    private static $table_name = 'OwnIp';

    private static $db = array(
        'IpAddress' => 'Varchar(50)',
        'Subnet' => 'Boolean',
        'Description' => 'Varchar(50)'
    );

    private static $has_one = array(
        'SiteConfig' => SiteConfig::class
    );

    private static $summary_fields = array(
        'IpAddress',
        'Subnet.Nice',
        'Description'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['IpAddress'] = _t("OwnIp.IPADDRESS", "IP address");
        $labels['Description'] = _t("OwnIp.DESCRIPTION", "Description");
        $labels['Subnet.Nice'] = _t("OwnIp.DESCRIPTION", "Subnet");

        return $labels;
    }

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $ipaddressfield = TextField::create('IpAddress', $l['IpAddress']);
        $ipaddressfield->setValue($_SERVER['REMOTE_ADDR']);
        if (!$this->ID){
            $ipaddressfield->setDescription(_t("OwnIp.YOUR_OWN_IP", "This is your current ip address, you may change this for another."));
        }

        $SubnetField = CheckboxField::create('Subnet', "Subnet");
        $SubnetField->setDescription(_t("OwnIp.SUBNET", "Enter an ipv6 subnet as: 2a02:120:112a: (2a02:120:112a::/48) and ipv4 without the 0."));

        $descriptionfield = TextField::create('Description', $l['Description']);
        $descriptionfield->setDescription(_t("OwnIp.DESCRIPTION_IS_OPTIONAL", "Description is optional"));

        GridFieldConfig::create()->addComponent(new GridFieldDeleteAction());

        $fields = new FieldList(
            $ipaddressfield,
            $SubnetField,
            $descriptionfield
        );

        return $fields;

    }

    public function validate() {
        $result = parent::validate();
        if(!filter_var($this->IpAddress, FILTER_VALIDATE_IP) && $this->Subnet == false) {
            $result->addError(_t('OwnIp.INVALID_IPADDRESS','This is not a valid ip address or check Subnet if it is.'));
        }
        return $result;
    }

}