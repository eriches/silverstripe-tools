<?php

use SilverStripe\Security\Security;

class Specs extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'php'
    );

    public function init() {
        parent::init();

        if (!Security::getCurrentUser()){
            return $this->httpError(404);
        }

    }

    public function php(){

        return phpinfo();

    }

}
